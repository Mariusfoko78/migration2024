<?php
// Chemin du fichier CSV
$csvFilePath = 'web_ex.csv';

// Vérification si le fichier existe
if (!file_exists($csvFilePath)) {
    die("Le fichier CSV '$csvFilePath' n'existe pas.");
}

// Ouvrir le fichier CSV en lecture
$file = new SplFileObject($csvFilePath, 'r');

// Créer un tableau pour stocker les données CSV
$csvData = [];

// Parcourir ligne par ligne
while (!$file->eof()) {
    // Lire une ligne du fichier CSV et la découper en tableau
    $data = $file->fgetcsv(',');

    // Vérifier si la lecture a réussi et que le tableau a au moins 3 éléments
    if ($data !== false && count($data) >= 3) {
        // Créer un tableau associatif avec les données
        $csvData[] = [
            'id' => $data[0],
            'name' => $data[1],
            'code' => $data[2],
        ];
    }
}

// Fermer le fichier
$file = null;

// Afficher les données du fichier CSV
foreach ($csvData as $row) {
    echo "ID: " . $row['id'] . ", Name: " . $row['name'] . ", Code: " . $row['code'] . "<br>";
}




















