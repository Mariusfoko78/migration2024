<?php

require_once '../config-dist.php';


// Vérifie si le script est exécuté en ligne de commande
if (php_sapi_name() !== 'cli') {
    echo "Ce script ne peut être exécuté qu'en ligne de commande.";
    die;
}

// Vérifie le nombre de paramètres passés en ligne de commande (doit être au moins 1)
if ($argc < 2) {
    echo "Nombre de paramètres insuffisant ! Au moins un paramètre (nom du fichier CSV) est requis.\n";
    die;
}

// Récupère le nom du fichier CSV à traiter depuis les arguments de la ligne de commande
$csvFileName = $argv[1];

// Vérifie si le fichier CSV spécifié existe
if (!file_exists($csvFileName)) {
    echo "Le fichier CSV '$csvFileName' n'existe pas.\n";
    die;
}

// Ouvrir le fichier CSV en lecture
$file = new SplFileObject($csvFileName, 'r');

// Parcourir le fichier ligne par ligne
while (!$file->eof()) {
    $row = $file->fgetcsv(',');

    // Vérifier si la ligne a été lue avec succès et contient des données
    if ($row !== false && count($row) >= 3) {
        // Traiter les données de la ligne (ici, nous supposons que la première colonne contient un ID, la deuxième le nom, et la troisième le code)
        $id = $row[0];
        $name = $row[1];
        $code = $row[2];

        //  Afficher les données
        echo " $id,  $name, $code\n";
    }
}

echo "Traitement du fichier CSV terminé.\n";






